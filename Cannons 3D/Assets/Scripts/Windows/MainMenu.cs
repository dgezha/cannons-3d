﻿using UnityEngine;
using UnityEngine.UI;
using Сontrollers = Cannons3D.Controllers.Controllers;

namespace Cannons3D.Windows
{
    public class MainMenu : Window
    {
        [SerializeField]
        private Button _startButton;

        [SerializeField]
        private Button _exitButton;

        protected override void Init()
        {
            _startButton?.onClick.AddListener(OnClickStart);
            _exitButton?.onClick.AddListener(OnClickExit);
        }

        private void OnClickStart()
        {
            Сontrollers.Instance.GameController.StartGameplay();
        }

        private void OnClickExit()
        {
            Application.Quit();
        }
    }
}
