﻿using Cannons3D.Utils;
using Cannons3D.Windows;
using System.Collections.Generic;
using UnityEngine;

namespace Cannons3D.Controllers
{
    public interface IUIController
    {
        void ShowWindow(GameWindowType type);
        void HideAllWindows();
    }

    public class UIController : Singleton<UIController>, IUIController
    {
        [SerializeField]
        private string _windowsPath;

        [SerializeField]
        private Transform _canvas;

        private readonly Dictionary<GameWindowType, string> _windowsNames = new Dictionary<GameWindowType, string>
        {
            {GameWindowType.MainMenu, "MainMenu" },
            {GameWindowType.GameplayWindow, "GameplayWindow" }
        };

        private Dictionary<GameWindowType, Window> _activeWindows = new Dictionary<GameWindowType, Window>();

        public void HideAllWindows()
        {
            foreach (var window in _activeWindows.Values)
            {
                window.Hide();
            }
        }

        public void ShowWindow(GameWindowType type)
        {
            HideAllWindows();

            if (!_activeWindows.ContainsKey(type))
            {
                _activeWindows[type] = Instantiate(LoadWindow(type), _canvas);
            }
            else
            {
                _activeWindows[type].Show();
            }
        }

        private Window LoadWindow(GameWindowType type)
        {
            return ResourcesLoader.LoadResource<Window>($"{_windowsPath}/{_windowsNames[type]}");
        }
    }

    public enum GameWindowType
    {
        MainMenu = 0,
        GameplayWindow = 1
    }
}
