﻿using System;
using UnityEngine;

namespace Cannons3D.Controllers
{
    public interface IInputController
    {
        event Action<Vector3> Shot;
        bool Active { get; set; }
    }

    public class InputController : Singleton<InputController>, IInputController
    {
        public event Action<Vector3> Shot;

        private readonly float radius = 0.25f;
        private readonly float distance = 10f;

        public bool Active { get ; set ; }

        private void Update()
        {
            if (Active)
            {
                CheckTouch();
                CheckMouse();
            }
        }

        private void CheckTouch()
        {
            if (Input.touchCount > 0)
            {
                var touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    Shot?.Invoke(GetHitPoint(touch.position));
                }
            }
        }

        private void CheckMouse()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Shot?.Invoke(GetHitPoint(Input.mousePosition));
            }
        }

        private Vector3 GetHitPoint(Vector2 position)
        {
            var camera = Camera.main;
            var ray = camera.ScreenPointToRay(new Vector3(position.x, position.y, 0));

            RaycastHit hit;
            if (Physics.SphereCast(ray, radius, out hit))
            {
                return hit.point;
            }

            return ray.GetPoint(distance);
        }
    }
}
