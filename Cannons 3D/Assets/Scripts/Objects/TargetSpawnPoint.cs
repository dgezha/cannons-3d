﻿using UnityEngine;
using Сontrollers = Cannons3D.Controllers.Controllers;

namespace Cannons3D.Objects
{
    public class TargetSpawnPoint : MonoBehaviour
    {
        private void Start()
        {
            Сontrollers.Instance?.LevelController.SetTargetSpawnPoint(this);
        }
    }
}
