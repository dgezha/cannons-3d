﻿using System;
using System.Collections.Generic;

namespace Cannons3D.Utils
{
    public class ReactiveProperty<T>
    {
        private T _value;

        private Action<T> _change;

        public T Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_value == null || !_value.Equals(value))
                {
                    _change?.Invoke(value);
                    _value = value;
                }
            }
        }

        public IDisposable Subscribe(Action<T> action)
        {
            _change += action;
            return new Disposable(() => _change -= action);
        }

        public ReactiveProperty() : this(default) { }

        public ReactiveProperty(T value)
        {
            _value = value;
        }
    }

    public class Disposable : IDisposable
    {
        private readonly Action _action;

        public Disposable(Action action)
        {
            _action = action;
        }

        public void Dispose()
        {
            _action?.Invoke();
        }
    }

    public class DisposableList : IDisposable
    {
        private List<IDisposable> _list = new List<IDisposable>();

        public void Add(IDisposable disposable)
        {
            _list.Add(disposable);
        }

        public void Dispose()
        {
            foreach (var disposable in _list)
            {
                disposable.Dispose();
            }
            _list.Clear();
        }
    }

    public static class DisposableExtension
    {
        public static void AddTo(this IDisposable disposable, DisposableList list)
        {
            list?.Add(disposable);
        }
    }
}
