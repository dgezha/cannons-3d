﻿using Cannons3D.Objects;
using Cannons3D.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Сontrollers = Cannons3D.Controllers.Controllers;

namespace Cannons3D.Controllers
{
    public interface ILevelController
    {
        ReactiveProperty<Level> CurrentLevel { get; }
        ReactiveProperty<Target> CurrentTarget { get; }
        ReactiveProperty<int> ProjectileNumber { get; }
        Level MaxLevel { get; }
        event Action Win;
        event Action Lose;
        void StartNewGame();
        void SetTargetSpawnPoint(TargetSpawnPoint point);
    }

    public class LevelController : Singleton<LevelController>, ILevelController
    {
        [SerializeField]
        private string _targetsPath;

        private TargetSpawnPoint _targetSpawnPoint;

        private readonly List<string> _targetsNames = new List<string>
        {
            "Target_1", 
            "Target_2", 
            "Target_3", 
            "Target_4", 
            "Target_5", 
            "Target_6", 
            "Target_7", 
            "Target_8", 
            "Target_9", 
            "Target_10"
        };

        private float _waitingTime = 5f;

        private IDisposable _disposable;

        public event Action Win;

        public event Action Lose;

        public ReactiveProperty<Level> CurrentLevel { get; } = new ReactiveProperty<Level>();

        public ReactiveProperty<Target> CurrentTarget { get; } = new ReactiveProperty<Target>();

        public Level MaxLevel => new Level(_targetsNames.Count - 1);

        public ReactiveProperty<int> ProjectileNumber { get; } = new ReactiveProperty<int>();

        public void StartNewGame()
        {
            _disposable?.Dispose();

            SetLevel(new Level(0));
            
            _disposable = ProjectileNumber.Subscribe(n =>
            {
                if (n <= 0)
                {
                    Сontrollers.Instance.InputController.Active = false;
                    StartCoroutine(CheckLose(_waitingTime));
                }
            });
        }

        public void SetTargetSpawnPoint(TargetSpawnPoint point)
        {
            _targetSpawnPoint = point;

            StartNewGame();
        }

        private void SetLevel(Level level)
        {
            Сontrollers.Instance.InputController.Active = true;
            CurrentLevel.Value = level;
            ProjectileNumber.Value = GetProjectileNumber(level);
            CurrentTarget.Value = CreateTargetOfLevel(CurrentLevel.Value);
            CurrentTarget.Value.Complete += () =>
            {
                StopAllCoroutines();

                Сontrollers.Instance.AnalyticsController.SendLevelComplete(CurrentLevel.Value);

                if (CurrentLevel.Value == MaxLevel)
                {
                    Win?.Invoke();
                    return;
                }
                SetLevel(level.Next);
            };
        }

        private int GetProjectileNumber(Level level)
        {
            return (level.Number + 1) * 3;
        }

        private Target CreateTargetOfLevel(Level level)
        {
            if (_targetSpawnPoint == null)
                throw new Exception("Target spawn point is missing on scene");

            return Instantiate(LoadTargetOfLevel(level), _targetSpawnPoint.transform);
        }

        private Target LoadTargetOfLevel(Level level)
        {
            return ResourcesLoader.LoadResource<Target>($"{_targetsPath}/{_targetsNames[level.Number]}");
        }

        private IEnumerator CheckLose(float waitingTime)
        {
            yield return new WaitForSeconds(waitingTime);
            if (ProjectileNumber.Value <= 0)
            {
                Lose?.Invoke();
            }
        }
    }
}
