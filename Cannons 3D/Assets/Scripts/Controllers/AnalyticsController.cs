﻿using Cannons3D.Plugins;
using Cannons3D.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cannons3D.Controllers
{
    public interface IAnalyticsController
    {
        void SendLevelComplete(Level level);
    }

    public class AnalyticsController : Singleton<AnalyticsController>, IAnalyticsController
    {
        [SerializeField]
        private FacebookPlugin _facebook;

        public void SendLevelComplete(Level level)
        {
            if (_facebook == null)
                throw new System.Exception("Facebook plugin is missing");

            _facebook.LogEventLevelAchieved(level.Number);
        }

        protected override void Awake()
        {
            base.Awake();
            _facebook.Init();
        }
    }
}
