﻿using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine;

namespace Cannons3D.Plugins
{
    public class FacebookPlugin : MonoBehaviour
    {
        public void Init()
        {
            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }
        }

        public void LogEventLevelAchieved(int level)
        {
            var parameters = new Dictionary<string, object>
            {
                { AppEventParameterName.Level, level}
            };
            FB.LogAppEvent(AppEventName.AchievedLevel, null, parameters);
        }

        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                FB.ActivateApp();
                // Continue with Facebook SDK
                // ...
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            }
            else
            {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }

        private void LogAppEvent(string name, Dictionary<string, object> parameters)
        {
            FB.LogAppEvent(name, null, parameters);
        }
    }
}
