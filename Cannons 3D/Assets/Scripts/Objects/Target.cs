﻿using UnityEngine;
using System.Collections.Generic;
using Cannons3D.Utils;
using System.Linq;
using System.Collections;
using System;

namespace Cannons3D.Objects
{
    [RequireComponent(typeof(BoxCollider))]
    public class Target : MonoBehaviour
    {
        [SerializeField]
        private float _checkInterval = 0.1f;

        [SerializeField]
        private float _waitInterval = 1f;

        public ReactiveProperty<int> MaxObjectsNumber { get; } = new ReactiveProperty<int>();

        public ReactiveProperty<int> CurrentObjectsNumber { get; } = new ReactiveProperty<int>();

        public event Action Complete;

        private IEnumerable<Bounds> _triggers;

        private void Awake()
        {
            var colliders = GetComponents<BoxCollider>();
            _triggers = colliders.Select(c => c.bounds);
        }

        private void Start()
        {
            MaxObjectsNumber.Value = IntersectionsCalculator.CountIntersectionsInBoxes(_triggers);
            StartCoroutine(CountTargets(_checkInterval));
        }

        private IEnumerator CountTargets(float interval)
        {
            while (true)
            {
                CurrentObjectsNumber.Value = IntersectionsCalculator.CountIntersectionsInBoxes(_triggers);
                yield return new WaitForSeconds(interval);
                if (CurrentObjectsNumber.Value == 0)
                {
                    yield return StartCoroutine(CheckComplete(_waitInterval));
                }
            }
        }

        private IEnumerator CheckComplete(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            if (CurrentObjectsNumber.Value == 0)
            {
                Complete?.Invoke();
                StopAllCoroutines();
                Destroy(gameObject);
            }
        }
    }
}
