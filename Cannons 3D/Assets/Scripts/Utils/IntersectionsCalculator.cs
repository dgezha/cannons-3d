﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Cannons3D.Utils
{
    public static class IntersectionsCalculator
    {
        public static int CountIntersectionsInBox(Bounds bounds)
        {
            return Physics.OverlapBox(bounds.center, bounds.extents, 
                Quaternion.identity, LayerMask.GetMask("Target")).Length;
        }

        public static int CountIntersectionsInBoxes(IEnumerable<Bounds> boxes)
        {
            return boxes.Sum(b => CountIntersectionsInBox(b));
        }
    }
}


