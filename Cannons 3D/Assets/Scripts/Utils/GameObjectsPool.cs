﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Collections;

namespace Cannons3D.Utils
{
    public class GameObjectsPool : MonoBehaviour
    {
        private GameObject _targetObject;

        private Transform _parentObject;

        private List<GameObject> _objects;

        private float _objectsLifetime;

        public void Init(GameObject obj, Transform parent, float lifetime)
        {
            _targetObject = obj;
            _parentObject = parent;
            _objectsLifetime = lifetime;
            _objects = new List<GameObject>();
        }

        public GameObject GetObject()
        {
            var obj = _objects.FirstOrDefault(o => !o.activeInHierarchy);
            if (obj != null)
            {
                obj.transform.position = _parentObject.position;
                obj.transform.rotation = _parentObject.rotation;
                obj.SetActive(true);
                HideObjectOverLifetime(obj);
                return obj;
            }
            return CreateNewObject();
        }

        private GameObject CreateNewObject()
        {
            var obj = Instantiate(_targetObject, _parentObject);
            _objects.Add(obj);
            HideObjectOverLifetime(obj);
            return obj;
        }

        private void HideObjectOverLifetime(GameObject obj)
        {
            StartCoroutine(HideObjectOverTime(obj, _objectsLifetime));
        }

        private IEnumerator HideObjectOverTime(GameObject obj, float time)
        {
            yield return new WaitForSeconds(time);
            obj.SetActive(false);
            obj.GetComponent<Rigidbody>()?.Sleep();
        }
    }
}
