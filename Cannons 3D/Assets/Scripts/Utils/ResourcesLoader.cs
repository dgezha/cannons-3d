﻿using UnityEngine;

namespace Cannons3D.Utils
{
    public static class ResourcesLoader
    {
        public static T LoadResource<T>(string path) where T : Object
        {
            var res = Resources.Load<T>(path);
            if (res == null)
                throw new System.Exception($"The resource of path {path} is missing");
            return res;
        }
    }
}
