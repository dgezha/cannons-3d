﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cannons3D.Utils
{
    public struct Level
    {
        public int Number { get; private set; }

        public Level Previous => new Level( Mathf.Max(0, Number - 1) );

        public Level Next => new Level(Number + 1);

        public Level(int level)
        {
            Number = level;
        }

        public override string ToString()
        {
            return (Number + 1).ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj is Level lvl)
            {
                return Number == lvl.Number;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Number.GetHashCode();
        }

        public static bool operator ==(Level l1, Level l2)
        {
            return l1.Equals(l2);
        }

        public static bool operator !=(Level l1, Level l2)
        {
            return !l1.Equals(l2);
        }
    }
}
