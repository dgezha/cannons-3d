﻿using Cannons3D.Utils;
using System;
using UnityEngine;

namespace Cannons3D.Windows
{
    public abstract class Window : MonoBehaviour, IDisposable
    {
        protected DisposableList _disposables = new DisposableList();

        public void Show()
        {
            Init();
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            Dispose();
        }

        protected virtual void Init() { }

        private void Awake()
        {
            Init();
        }

        public virtual void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
