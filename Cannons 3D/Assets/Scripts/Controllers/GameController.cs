﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Сontrollers = Cannons3D.Controllers.Controllers;
using System.Collections;
using System.Collections.Generic;

namespace Cannons3D.Controllers
{
    public interface IGameController
    {
        void StartNewGame();
        void StartGameplay();
    }

    public class GameController : Singleton<GameController>, IGameController
    {
        private float _waitingTime = 3f;

        private readonly Dictionary<Scene, string> _scenesNames = new Dictionary<Scene, string>
        {
            { Scene.Init, "Init"},
            { Scene.Game, "Game"}
        };

        private IUIController _uIController;
        private ILevelController _levelController;

        private void Start()
        {
            _uIController = Сontrollers.Instance.UIController;
            _levelController = Сontrollers.Instance.LevelController;
        }

        public void StartNewGame()
        {
            SceneManager.LoadScene(_scenesNames[Scene.Init]);
            _uIController.ShowWindow(GameWindowType.MainMenu);
        }

        public void StartGameplay()
        {
            SceneManager.LoadScene(_scenesNames[Scene.Game]);
            _uIController.ShowWindow(GameWindowType.GameplayWindow);

            _levelController.Win += () => StartCoroutine(NewGameDelayed(_waitingTime));
            _levelController.Lose += () => StartCoroutine(NewGameDelayed(_waitingTime));
        }

        private IEnumerator NewGameDelayed(float time)
        {
            yield return new WaitForSeconds(time);
            StartNewGame();
        }
    }

    public enum Scene
    {
        Init = 0,
        Game = 1
    }
}
