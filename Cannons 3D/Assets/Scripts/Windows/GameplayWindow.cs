﻿using Cannons3D.Utils;
using UnityEngine;
using UnityEngine.UI;
using Сontrollers = Cannons3D.Controllers.Controllers;
using Cannons3D.Controllers;

namespace Cannons3D.Windows
{
    public class GameplayWindow : Window
    {
        [SerializeField]
        private Text _projectileLabel;

        [SerializeField]
        private Text _currentLevelLabel;

        [SerializeField]
        private Text _nextLevelLabel;

        [SerializeField]
        private Slider _progress;

        [SerializeField]
        private GameObject _winLabel;

        [SerializeField]
        private GameObject _loseLabel;

        private ILevelController _levelController;
        private Level _maxLevel;

        protected override void Init()
        {
            _levelController = Сontrollers.Instance.LevelController;
            _maxLevel = _levelController.MaxLevel;

            _winLabel.SetActive(false);
            _loseLabel.SetActive(false);

            _levelController.CurrentLevel.Subscribe(SetLevelLabels).AddTo(_disposables);
            _levelController.ProjectileNumber.Subscribe(SetProjectileNumber).AddTo(_disposables);
            _levelController.CurrentTarget.Subscribe(target =>
            {
                target.MaxObjectsNumber.Subscribe(SetMaxObjectsCount).AddTo(_disposables);
                target.CurrentObjectsNumber.Subscribe(SetCurrentObjectsNumber).AddTo(_disposables);
            }).AddTo(_disposables);

            _levelController.Win += ShowWinLabel;
            _levelController.Lose += ShowLoseLabel;
        }

        private void SetLevelLabels(Level level)
        {
            _currentLevelLabel.text = level.ToString();
            _nextLevelLabel.text = level != _maxLevel ? level.Next.ToString() : "Win!";
        }

        private void SetProjectileNumber(int number)
        {
            _projectileLabel.text = $"x {number}";
        }

        private void SetMaxObjectsCount(int count)
        {
            _progress.maxValue = count;
        }

        private void SetCurrentObjectsNumber(int count)
        {
            _progress.value = _progress.maxValue - count;
        }

        private void ShowWinLabel()
        {
            _winLabel.SetActive(true);
        }

        private void ShowLoseLabel()
        {
            _loseLabel.SetActive(true);
        }

        public override void Dispose()
        {
            base.Dispose();
            _levelController.Win -= ShowWinLabel;
            _levelController.Lose -= ShowLoseLabel;
        }
    }
}
