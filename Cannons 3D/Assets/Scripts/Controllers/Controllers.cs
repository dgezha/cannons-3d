﻿using UnityEngine;

namespace Cannons3D.Controllers
{
    public class Controllers : Singleton<Controllers>
    {
        [SerializeField]
        private GameController _gameController;

        [SerializeField]
        private LevelController _levelController;

        [SerializeField]
        private UIController _uiController;

        [SerializeField]
        private InputController _inputController;

        [SerializeField]
        private AnalyticsController _analyticsController;

        public IGameController GameController => _gameController;

        public ILevelController LevelController => _levelController;

        public IUIController UIController => _uiController;

        public IInputController InputController => _inputController;

        public IAnalyticsController AnalyticsController => _analyticsController;

        private void Start()
        {
            GameController.StartNewGame();
        }
    }
}
