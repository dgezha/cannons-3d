﻿using Cannons3D.Utils;
using UnityEngine;
using System.Collections;
using Сontrollers = Cannons3D.Controllers.Controllers;
using Cannons3D.Controllers;

namespace Cannons3D.Objects
{
    public class Cannon : MonoBehaviour
    {
        [SerializeField]
        private Transform _projectileSpawn;

        [SerializeField]
        private GameObject _projectile;

        [SerializeField]
        private GameObjectsPool _projectilesPool;

        [SerializeField]
        private float _force;

        [SerializeField]
        private float _cooldown;

        [SerializeField]
        private float _projectileLifetime;

        private bool _canShoot = true;

        private ILevelController _levelController;

        private void Awake()
        {
            _projectilesPool?.Init(_projectile, _projectileSpawn, _projectileLifetime);

            _levelController = Сontrollers.Instance.LevelController;
        }

        private void Start()
        {
            if (Сontrollers.Instance != null)
                Сontrollers.Instance.InputController.Shot += Shoot;
        }

        private void Shoot(Vector3 point)
        {
            if (!_canShoot) return;

            GameObject projectile;
            if (_projectilesPool != null)
            {
                projectile = _projectilesPool.GetObject();
            }
            else
            {
                projectile = Instantiate(_projectile, _projectileSpawn);
                Destroy(projectile, _projectileLifetime);
            }
            projectile.GetComponent<Rigidbody>().AddForce((point - _projectileSpawn.position).normalized * _force);

            _levelController.ProjectileNumber.Value--;

            ShootDelay();
        }

        private void ShootDelay()
        {
            _canShoot = false;
            StartCoroutine(RefreshCooldown());
        }

        private IEnumerator RefreshCooldown()
        {
            yield return new WaitForSeconds(_cooldown);
            _canShoot = true;
        }

        public void OnDestroy()
        {
            if (Сontrollers.Instance != null)
                Сontrollers.Instance.InputController.Shot -= Shoot;
        }
    }
}
